import { Body, Controller, Delete, Get,Param,Post, Put, Query, Redirect } from '@nestjs/common';
import { create } from 'domain';
import { query } from 'express';
import { version } from 'os';
import { CreateCatDto } from 'src/dto/create-cat.dto';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';


@Controller('cats')
export class CatsController {
    constructor (private catsService:CatsService){}

@Post()
async create(@Body()createCatDto:CreateCatDto){
this.catsService.create(createCatDto);
}

@Get()
async findAll():Promise<Cat[]>{
    return this.catsService.findAll();
}
}

