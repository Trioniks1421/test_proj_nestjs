import { Module } from '@nestjs/common';
import { AdminController } from './admin/admin.controller';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';


@Module({
  imports: [CatsModule],
  controllers: [AppController,AdminController],
  providers: [AppService],
})
export class AppModule {}
